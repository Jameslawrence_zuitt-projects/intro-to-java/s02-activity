package com.villanueva.s02activity;

import java.util.Scanner;

public class LeapYearIdentifier {
    public static void main(String[] args){
        Scanner year = new Scanner(System.in);

        System.out.println("==================================================");
        System.out.println("Leap Year Identifier\n");

        //input year field.
        System.out.println("Enter a Year:");
        int leapYear = Integer.parseInt(year.nextLine());

        // leap year processor
        int verifyYear = leapYear%4;

        if(verifyYear == 0){
            System.out.println("Leap Year ["+leapYear+"]");
        }else{
            System.out.println("Not a Leap Year ["+leapYear+"]");
        }

        System.out.println("==================================================");
    }
}
